try:
    from .shared import *
except ImportError:
    pass

DEBUG = False

WSGI_APPLICATION = 'consensus_api.wsgi.application'

ALLOWED_HOSTS = ['*']

DATABASES = {
    'default': {
            'ENGINE': 'django.db.backends.mysql',
            'HOST': env('DB_HOST_STRING'),
            'NAME': env('DB_NAME'),
            'USER': env('DB_USER'),
            'PASSWORD': env('DB_PASS'),
        },
}