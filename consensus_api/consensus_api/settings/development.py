try:
    from .shared import *
except ImportError as exc:
    raise ImportError(exc.args) from exc

DEBUG = True

WSGI_APPLICATION = 'consensus_api.wsgi.application'

ALLOWED_HOSTS = ['*']

DATABASES = {
        'default': {
            'ENGINE': 'django.db.backends.mysql',
            'NAME': env('DB_NAME'),
            'USER': env('DB_USER'),
            'PASSWORD': env('DB_PASS'),
            'HOST': env('DB_HOST'),
            'PORT': env('DB_PORT')
        },
        # 'default': {  # Local Run
        #     'ENGINE': 'django.db.backends.mysql',
        #     'NAME': 'your_name',
        #     'USER': 'root',
        #     'PASSWORD': 'your_pass',
        #     'HOST': '127.0.0.1',
        #     'PORT': '3306'
        # }
    }