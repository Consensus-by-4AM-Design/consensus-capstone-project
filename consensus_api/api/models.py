from django.db import models

# Create your models here.

# class testTable2312 (models.Model):
#         title = models.CharField(max_length=100)
#         genreID = models.CharField(max_length=100)
#         releaseDate = models.DateTimeField(auto_now_add=True)

#         def __str__(self):
#             return self.title

class Movie(models.Model):
    tmdb_id = models.IntegerField(unique=True)
    hive_id = models.IntegerField(unique=True, null=True)
    title = models.CharField(max_length=100)
    review_score = models.DecimalField(max_digits=5, decimal_places=2)
    number_posts = models.IntegerField(default=0)
    release_date = models.CharField(max_length=10, default='1000-10-10')
    genre_ids = models.CharField(max_length=200, default='28,12,16')
    backdrop_path = models.CharField(max_length=200, null=True)
    poster_path = models.CharField(max_length=200, null=True)


class Reddit_Post(models.Model):
    movie = models.ForeignKey(Movie, on_delete=models.CASCADE)
    reddit_post_id = models.CharField(unique=True, max_length=10)
    reddit_comment_url = models.CharField(max_length=100, null=True)
    username = models.CharField(max_length=100)
    comment = models.CharField(max_length=400)
    score = models.IntegerField(default=0)
    is_longer = models.BooleanField(default=False)

class Common_Phrase(models.Model):
    phrase = models.CharField(max_length=100)


class Common_Word(models.Model):
    word = models.CharField(max_length=31)
    

class Common_Phrase_Movie(models.Model):
    movie = models.ForeignKey(Movie, on_delete=models.CASCADE)
    phrase = models.ForeignKey(Common_Phrase, on_delete=models.CASCADE)
    count = models.IntegerField(default=0)


class Common_Word_Movie(models.Model):
    movie = models.ForeignKey(Movie, on_delete=models.CASCADE)
    word = models.ForeignKey(Common_Word, on_delete=models.CASCADE)
    count = models.IntegerField(default=0)
