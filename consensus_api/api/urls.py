from django.urls import path
from .views import *
from .update.runner import run_update
urlpatterns = [
    path('movies/popular/<int:page>/',popular),
    path('movies/newReleases/<int:page>/',new_releases),
    path('movies/topRated/<int:page>/',top_rated),
    path('movies/genre/<int:genre_id>/<int:page>/',genre),
    path('movies/details/<int:movie_id>/',details),
    path('movies/search/',search),
    path('movies/update/<slug:password>/',run_update)
]