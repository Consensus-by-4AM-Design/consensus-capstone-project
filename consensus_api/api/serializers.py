from rest_framework import serializers
from .models import *

class MovieSerializer(serializers.ModelSerializer):
    CID = serializers.IntegerField(source='id')
    id = serializers.IntegerField(source='tmdb_id')
    review_percentage = serializers.DecimalField(source='review_score', max_digits=5, decimal_places=2)
    class Meta:
        model = Movie
        fields = ['CID', 'id', 'title', 'review_percentage',\
             'number_posts', 'release_date', 'genre_ids', 'backdrop_path', 'poster_path']

class RedditPostSerializer (serializers.ModelSerializer):
    class Meta:
        model = Reddit_Post
        fields = ['movie_id', 'reddit_post_id','reddit_comment_url','username','comment','score', 'is_longer']

class CommonPhraseSerializer (serializers.ModelSerializer):
    class Meta:
        model = Common_Phrase
        fields = ['phrase']

class CommonWordSerializer (serializers.ModelSerializer):
    class Meta:
        model = Common_Word
        fields = ['word']

class CommonPhraseMovieSerializer (serializers.ModelSerializer):
    phrase = serializers.CharField(source='phrase.phrase', read_only=True)
    class Meta:
        model = Common_Phrase_Movie
        fields = [ 'movie_id', 'phrase', 'count']

class CommonWordMovieSerializer (serializers.ModelSerializer):
    word = serializers.CharField(source='word.word', read_only=True)
    class Meta:
        model = Common_Word_Movie
        fields = ['movie_id', 'word', 'count']