# Generated by Django 3.1.5 on 2021-03-15 17:00

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0002_auto_20210315_1053'),
    ]

    operations = [
        migrations.RenameField(
            model_name='commonphrasemovie',
            old_name='movie_id',
            new_name='movie',
        ),
        migrations.RenameField(
            model_name='commonphrasemovie',
            old_name='phrase_id',
            new_name='phrase',
        ),
        migrations.RenameField(
            model_name='commonwordmovie',
            old_name='movie_id',
            new_name='movie',
        ),
        migrations.RenameField(
            model_name='commonwordmovie',
            old_name='word_id',
            new_name='word',
        ),
        migrations.RenameField(
            model_name='redditpost',
            old_name='movie_id',
            new_name='movie',
        ),
    ]
