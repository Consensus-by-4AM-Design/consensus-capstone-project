# Generated by Django 3.1.5 on 2021-03-25 10:49

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0006_auto_20210315_1139'),
    ]

    operations = [
        migrations.AddField(
            model_name='movie',
            name='number_posts',
            field=models.IntegerField(default=0),
        ),
        migrations.AlterField(
            model_name='common_phrase_movie',
            name='count',
            field=models.IntegerField(default=0),
        ),
        migrations.AlterField(
            model_name='common_word_movie',
            name='count',
            field=models.IntegerField(default=0),
        ),
        migrations.AlterField(
            model_name='reddit_post',
            name='score',
            field=models.IntegerField(default=0),
        ),
    ]
