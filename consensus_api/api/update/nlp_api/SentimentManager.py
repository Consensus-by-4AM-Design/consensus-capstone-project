# Imports the Google Cloud client library
import sys
import pandas as pd
from google.cloud import language_v1
from .TextProcessor import removeStopWords, countCommonWords

USE_GCP_API = False

def authGCPClient():
    return language_v1.LanguageServiceClient.from_service_account_json(
        "./api/update/nlp_api/env/nlp_api_key.json")

def getSentiment(comment, client = ''):
    filteredText = removeStopWords(comment)
    document = convertTextToDocument(filteredText)
    sentiment = requestSentiment(document, client)
    if USE_GCP_API:
        return sentiment.score, sentiment.magnitude
    return sentiment['score'], sentiment['magnitude']

def requestSentiment(document, client = ''):
    if USE_GCP_API:
        print('using google api')
        # sentiment = client.analyze_sentiment(
        #     request={'document': document}).document_sentiment
        # return sentiment
    return {'score': -69, 'magnitude': -69}

def convertTextToDocument(text):
    document = language_v1.Document(
        content=text, type_=language_v1.Document.Type.PLAIN_TEXT)
    return document


def process_comments(comments, csv_name='comment_training_data'):
    client = ''
    if USE_GCP_API:
        client = authGCPClient()
    comments['sentiment'], comments['magnitude'] = zip(*comments['comment'].apply(getSentiment, args=(client,)))
    comments.to_csv(f"./api/update/nlp_api/nlp_training_data/{csv_name}.csv", index=False, header=True)

def calculate_overall_sentiment(comments_df, movies_df):
    movies = movies_df['title']
    index = 0
    for movie in movies:
        movie_comments = comments_df.loc[comments_df['title'] == movie]
        movies_df.at[index, 'review_score'] = round(50 * movie_comments['sentiment'].mean() + 50, 2)  # as a percent
        movies_df.at[index, 'number_posts'] = len(movie_comments)
        index = index + 1
