import sys
import nltk
from nltk.corpus import stopwords
from nltk.corpus import wordnet as wn
from nltk.tokenize import word_tokenize
from nltk.probability import FreqDist
from nltk.tag import pos_tag
import pandas as pd

# only have to run these once, if u get error
nltk.download('stopwords')
nltk.download('punkt')
nltk.download('wordnet')
nltk.download('averaged_perceptron_tagger')

def removeNouns(text):
    tagged_sent = pos_tag(text.split())
    pos_tags = ['JJ', 'RBR']
    propernouns = [word for word, pos in tagged_sent if pos in pos_tags]
    return ' '.join(propernouns)

def removeStopWords(text):
    stopWords = set(stopwords.words('english'))
    wordTokens = word_tokenize(text)
    filteredText = [w for w in wordTokens if not w in stopWords and w.isalnum()]
    filteredSentence = ' '.join(filteredText)
    return filteredSentence

def countCommonWords(text):
    filteredSentence = removeNouns(removeStopWords(text.lower()))
    wordTokens = word_tokenize(filteredSentence)
    wordFrequencies = FreqDist(wordTokens)
    # only if the frequency of word is greater than 3.
    filterWords = [(m, n) for m, n in wordFrequencies.items() if n > 1]
    return sorted(filterWords, key=lambda item: item[1], reverse=True)

def get_common_words(comments_df, movies_df):
    movies = movies_df['title']
    index = 0
    for movie in movies:
        movie_comments = comments_df[comments_df.title == movie]
        movie_common_words = pd.Series(
            countCommonWords(' '.join(movie_comments['comment']))
            )[:10]
        movies_df['words'] = movies_df['words'].astype(object)
        movies_df['word_counts'] = movies_df['word_counts'].astype(object)
        movies_df.at[index, 'words'] = [(word) for word, word_count in movie_common_words]
        movies_df.at[index, 'word_counts'] = [(word_count) for word, word_count in movie_common_words]
        index = index + 1