import pandas as pd


def get_posts(reddit, movies_df):
    movie_names = movies_df.title.tolist()
    tmdb_ids = movies_df.id.tolist()
    posts = pd.DataFrame(columns=['title', 'tmdb_id', 'threads'])

    i = 0
    for movie in movie_names:
        # Steady discussion in r/movies
        post_list = []
        search = "official discussion: " + movie
        for submission in reddit.subreddit("movies").search(query=search, sort="relevance", limit=1):
            post_list.append(submission.id)

        # r/Badmovies
        search = '"' + movie + '" review'
        for submission in reddit.subreddit("Badmovies").search(query=search, sort="top", limit=3):
            if submission.num_comments > 0:
                post_list.append(submission.id)

        # r/all
        search = "title:" + movie + " AND title:review AND selftext:movie"
        for submission in reddit.subreddit("all").search(query=search, sort="relevance", limit=2):
            if submission.num_comments > 0:
                post_list.append(submission.id)

        movie_posts = {
            'title': movie,
            'tmdb_id': tmdb_ids[i],
            'threads': post_list
        }
        posts = posts.append(movie_posts, ignore_index=True)
        i += 1

    return posts
