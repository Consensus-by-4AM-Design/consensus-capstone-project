from .reddit_api_session import reddit_session
from .new_movies_helper import get_movies
from .comment_gatherer import get_comments
from .review_thread_helper import get_posts
from .hive_db_broker import hive_store_comments
from .sql_update_helper import update_sql_db
from .nlp_api.SentimentManager import process_comments
from .nlp_api.SentimentManager import calculate_overall_sentiment
from .nlp_api.TextProcessor import get_common_words
from .top_reddit_posts_helper import get_top_reddit_posts
import pandas as pd
from django.http import HttpResponse, HttpResponseNotFound


def run_update(request, password):
    if(password == '31560ED96185'):
        reddit = reddit_session()

        # Find movie list to Process
        # in - N/A
        # out - list dict. of the top movies w/ tmdb ids e.g.
        #   [{
        #     "backdrop_path": "/pcDc2WJAYGJTTvRSEIpRZwM3Ola.jpg",
        #     "genre_ids": [
        #       28,
        #       12,
        #       14,
        #       878
        #     ],
        #     "id": 791373,     # NOTE: TMDB ID
        #     "poster_path": "/tnAuB8q5vv7Ax9UAEje5Xi4BXik.jpg",
        #     "release_date": "2021-03-18",
        #     "title": "Zack Snyder's Justice League"
        #   }, ...]
        movies = get_movies()


        # in - dict. of the top movies w/ tmdb ids e.g. {"Mad max":"id##", "Cats":"id##", "Tron":"id##"}
        # Save ^^^ as dataframe
        # out - review_thread_ids  e.g. {"Mad max":["post_id##", "post_id##"], "Cats":["post_id##"]}
        movies_df = pd.DataFrame(columns=["backdrop_path", "genre_ids", "id", "poster_path", "release_date", "title",
                                        "words", "word_counts", "review_score", "number_posts"])

        for movie in movies:
            movies_df = movies_df.append(movie, ignore_index=True)


        # Get Review Thread IDs for comment gatherer
        review_thread_df = get_posts(reddit, movies_df)

        # Gather Comments from posts
        # in - review_thread_df  e.g. | 'title' | 'tmdb_id' | 'threads'
        #                             | 'Cats'  | '3298s23' | ['1398dfhs', '7983dh']
        # out - comments dataframe {'comment': comment.body, 'id': comment.id, 'score': comment.score, 'movie': movie}
        # e.g. comments_df = 'comment' | 'tmdb_id' | 'reddit_post_id' | 'score' | 'reddit_comment_url' | 'username' | 'title'
        #                    'blah'    | '2e235'   | '234sfdh'        | 340     | 'r/.../ksdjhf20'     | 'catBoi21' | 'Cats'
        comments_df = get_comments(reddit, review_thread_df)

        # in - comments dataframe {'comment': comment.body, 'id': comment.id, 'score': comment.score, 'movie': movie}, csv_name: unique name to store google data
        # out - updated comments dataframe {'comment': comment.body, 'id': comment.id,
        #                                   'score': comment.score, 'movie': movie, 'sentiment':sentiment, 'magnitude': magnitude}
        csv_name = ''
        process_comments(comments_df, csv_name)

        # out - fills movies_df with {"review_score": score}
        calculate_overall_sentiment(comments_df, movies_df)



        # TODO: Store comments with scores in Hive DB - Jesse (Stretch goal)
        # in - updated comments dataframe {'comment': comment.body, 'id': comment.id,
        #                                  'score': comment.score, 'movie': movie, 'sentiment':sentiment}
        # out - Hive Storage Confirmation
        # result = hive_store_comments(comments_df, movies_df)
        # if result != 0:
        #     print("error") # TODO: Proper break the pipeline if the hive storing doesn't work

        # Update w/ movies_df fields change
        #   Notably: 'words': [100 top words in order left to right as a list],
        #   'word_count': [list of word counts in respective order]
        # in - comments & movies dataframe (for the movie list)
        # out - fills movies_df with{words': [10 top words],
        #                       'word_count': [list of word counts in respective order]}
        get_common_words(comments_df, movies_df)


        # get the top posts per movie
        # in - comments_df
        # Out  - top_reddit_posts_df (3 per movie)
        #   [{
        #       "tmdb_id": 1, # TMDB
        #       "reddit_post_id": "co6c6in",
        #       "username": "randomuser2",
        #       "comment": "Just watched it, amazing. Fletcher was really terrifying and at the I was waiting, ...",
        #       "score": 3,
        #       "is_longer": false
        #   }, ...
        #   ]
        top_reddit_posts_df = get_top_reddit_posts(comments_df)


        # in - movies_df / movies dataframe (for the movie list),  e.g.
        # "backdrop_path" | "genre_ids" | "id" | "overview"
        # [{
        #   "backdrop_path": "/fRGxZuo7jJUWQsVg9PREb98Aclp.jpg",
        #   "genre_ids": [
        #       28,
        #       12,
        #       14,
        #       878
        #   ],
        #   "id": 244786,
        #   "overview": "Under the direction of a ruthless instructor, a talented young drummer begins to pursue perfection at any cost, even his humanity.",
        #   "poster_path": "/6uSPcdGNA2A6vJmCagXkvnutegs.jpg",
        #   "release_date": "2014-10-10",
        #   "title": "Whiplash",
        #   "review_score": "88.21", # percentage, 2 decmial places
        #   "number_posts": 0,
        #   "common_words": [ # 10 Max
        #       "WORD", "ball", "dog"
        #   ],
        #   "common_words_count": [
        #       6, 4, 2
        #   ]
        # }, ...]
        # out - Confirmation of SQL storage
        update_sql_db(movies_df, top_reddit_posts_df)
        return HttpResponse('')  # Return blank html so that there are no errors
    return HttpResponseNotFound('')
