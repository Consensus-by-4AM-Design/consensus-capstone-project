import praw
import json

def reddit_session():
    with open('./api/update/reddit_api_auth.json') as f:
        reddit_auth = json.load(f)
    session = praw.Reddit(
        client_id=reddit_auth["client_id"],
        client_secret=reddit_auth["client_secret"],
        user_agent=reddit_auth["user_agent"],
        password=reddit_auth["password"],
        username=reddit_auth["username"]
    )
    return session


# Use:
# from MLStartingData.reddit_api_session import reddit_session
# reddit = reddit_session()
