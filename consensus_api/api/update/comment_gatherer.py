import pandas as pd


def get_username(comment):
    if comment.author:
        return comment.author.name
    else:
        return "Anonymous"


def get_comments(reddit, threads_df):
    null_comments = ["[removed]", "[deleted]"]
    commments_dataset = pd.DataFrame(columns=['comment', 'tmdb_id', 'reddit_post_id', 'score', 'reddit_comment_url',
                                              'username', 'title'])
    for index, movie in threads_df.iterrows():
        title = movie['title']
        movie_id = movie['tmdb_id']
        threads = movie['threads']
        for thread in threads:
            post = reddit.submission(id=thread)
            post.comments.replace_more(limit=40)

            for comment in post.comments.list():
                if hasattr(comment, 'body') and comment.body not in null_comments:
                    username = get_username(comment)

                    data = {
                        'comment': comment.body,
                        'tmdb_id': movie_id,
                        'reddit_post_id': comment.id,
                        'score': comment.score,
                        'reddit_comment_url': post.permalink + comment.id,
                        'username': username,
                        'title': title
                    }
                    commments_dataset = commments_dataset.append(data, ignore_index=True)

    return commments_dataset
