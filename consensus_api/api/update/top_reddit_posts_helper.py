import pandas as pd
import re

def deEmojify(text):
    regrex_pattern = re.compile(pattern = "["
        u"\U0001F600-\U0001F64F"  # emoticons
        u"\U0001F300-\U0001F5FF"  # symbols & pictographs
        u"\U0001F680-\U0001F6FF"  # transport & map symbols
        u"\U0001F1E0-\U0001F1FF"  # flags (iOS)
                           "]+", flags = re.UNICODE)
    return regrex_pattern.sub(r'',text)

def get_top_reddit_posts(comments_df):
    movies = comments_df.title.unique()
    top_movie_posts = pd.DataFrame(columns=["tmdb_id", "reddit_post_id", "username", "comment", "score", "reddit_comment_url"])
    for movie in movies:
        movie_comments = comments_df.loc[comments_df['title'] == movie]
        if movie_comments.empty:  # Skip if no comments fond on a movie
            continue
        top_posts = movie_comments.nlargest(3, 'score')
        for index, post in top_posts.iterrows():
            top_movie_post = {
                "tmdb_id": post.tmdb_id,  # TMDB
                "reddit_post_id": post.reddit_post_id,
                "username": post.username,
                "comment": deEmojify(post.comment),
                "score": post.score,
                "reddit_comment_url": post.reddit_comment_url
            }
            top_movie_posts = top_movie_posts.append(top_movie_post, ignore_index=True)
    return top_movie_posts