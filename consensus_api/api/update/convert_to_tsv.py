import pandas as pd

df = pd.read_csv('ml_training_data.csv')
df.columns = ['comment', 'id', 'score', 'movie', 'sentiment', 'sarcastic', 'relevant']


def clean_text(text):
    value = text.replace('\t', ' ')
    value = value.replace('\n', ' ')
    return value.strip()


df['comment'] = list(map(clean_text, df['comment']))
df['sarcastic'] = list(map(clean_text, df['sarcastic']))
df['relevant'] = list(map(clean_text, df['relevant']))
df['sentiment'] = list(map(clean_text, df['sentiment']))


df.to_csv('ml_training_data.tsv', index=False, sep='\t')
