import tmdbconfig as cfg
from ..views import trim_unwanted_data_list
from ..models import Movie
import requests


def get_movies(page=1):
    movie_data_list = list()
    while len(movie_data_list) <= 20:
        response = requests.get('https://api.themoviedb.org/3/movie/popular?api_key=' \
        + cfg.tmDB['api_key'] + '&language=en-US&page=' + str(page))
        movie_data = response.json()
        trim_unwanted_data_list(movie_data)
        for i in movie_data['results']:
            if Movie.objects.filter(tmdb_id = i['id']):
                movie_data['results'].remove(i)
        movie_data_list.extend(movie_data['results'])
        page +=1
    return movie_data_list[:5]
