from ..models import Movie, Reddit_Post, Common_Phrase, Common_Phrase_Movie, Common_Word, Common_Word_Movie
from datetime import datetime

def clean_up_movie(movie_data):
    movie_data['genre_ids'] = movie_data['genre_ids'].strip('][').split(', ')
    # movie_data['word_counts'] = movie_data['word_counts'].strip('][').split(', ')
    # movie_data['words'] = movie_data['words'].strip('][').split(', ')
    for i in range(len(movie_data['words'])):
        movie_data['words'][i] = movie_data['words'][i][1:-1]
    for i in range(len(movie_data['genre_ids'])):
        movie_data['genre_ids'][i] = int(movie_data['genre_ids'][i])
    if len(movie_data['word_counts']) != 0 and movie_data['word_counts'][0] != '':
        for i in range(len(movie_data['word_counts'])):
            movie_data['word_counts'][i] = int(movie_data['word_counts'][i])
    date = datetime.strptime(movie_data['release_date'], "%m/%d/%Y")
    movie_data['release_date'] = date.strftime('%Y-%m-%d')

def update_sql_db(movie_data_list_df, top_reddit_posts_df):
    '''this function is responsible for updating the sql database via a given dictionary/dataframe'''
    movie_data_list = movie_data_list_df.T.to_dict().values()
    top_reddit_posts = top_reddit_posts_df.T.to_dict().values()
    for movie_data in movie_data_list:
        clean_up_movie(movie_data)
        genre_ids = ','.join([str(i) for i in movie_data['genre_ids']])
        current_movie = Movie(tmdb_id=movie_data['id'], hive_id = None, \
            title=movie_data['title'], review_score=movie_data['review_score'], \
                number_posts=movie_data['number_posts'], release_date=movie_data['release_date'], \
                    genre_ids=genre_ids, backdrop_path=movie_data['backdrop_path']\
                        ,poster_path=movie_data['poster_path'])
        if not Movie.objects.filter(tmdb_id=movie_data['id']):
            current_movie.save()
        else:
            current_movie = Movie.objects.filter(tmdb_id=movie_data['id'])[0]

        for reddit_post_data in (relevant_posts for relevant_posts in top_reddit_posts if relevant_posts['tmdb_id'] == current_movie.tmdb_id):
            is_longer = False
            if len(reddit_post_data['comment']) >= 400:
                is_longer = True
                reddit_post_data['comment'] = reddit_post_data['comment'][:400]
            current_reddit_post = Reddit_Post(movie=current_movie, reddit_post_id=reddit_post_data['reddit_post_id'], \
                username=reddit_post_data['username'], comment=reddit_post_data['comment'], \
                score=reddit_post_data['score'], is_longer=is_longer, reddit_comment_url=reddit_post_data['reddit_comment_url'])
            if not Reddit_Post.objects.filter(reddit_post_id=reddit_post_data['reddit_post_id']):
                current_reddit_post.save()

        # '''needs to be fixed, we will get duplicated phrases here'''
        # for common_phrase_data in movie_data['common_phrases']:
        #     current_common_phrase = Common_Phrase(phrase=common_phrase_data['phrase'])
        #     '''check if phrase is already in db before saving.'''
        #     current_common_phrase.save()
        #     current_common_phrase_movie = Common_Phrase_Movie(movie=current_movie, \
        #         phrase=current_common_phrase, count=common_phrase_data['count'])
        #     current_common_phrase_movie.save()

        #TODO Change the way this works based off instructions in runner.py
        for common_word_data_index in range(len(movie_data['words'])):
            current_common_word = Common_Word(word=movie_data['words'][common_word_data_index])

            # check if word already in db, if not, insert
            if not Common_Word.objects.filter(word=movie_data['words'][common_word_data_index]):
                current_common_word.save()
            else:
                current_common_word = Common_Word.objects.filter(word=movie_data['words'][common_word_data_index])[0]

            current_common_word_movie = Common_Word_Movie(movie=current_movie, \
                word=current_common_word, count=movie_data['word_counts'][common_word_data_index])
            if not Common_Word_Movie.objects.filter(word=current_common_word, movie=current_movie):
                current_common_word_movie.save()
