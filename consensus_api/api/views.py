import logging
from django.shortcuts import render
from django.http import JsonResponse, HttpResponseNotFound
import requests
import tmdbconfig as cfg
from .models import *
from .serializers import *
import math

logger = logging.getLogger(__name__)

# Create your views here.
def popular(request, page):
    '''responsible for the popular url call'''
    if request.method =='GET':
        response = requests.get('https://api.themoviedb.org/3/movie/popular?api_key=' \
        + cfg.tmDB['api_key'] + '&language=en-US&page=' + str(page))
        movie_data = response.json()
        trim_unwanted_data_list(movie_data)
        add_consensus_data_list(movie_data)
        return JsonResponse(movie_data)
    return None

def new_releases(request, page):
    '''responsible for the new_releases url call'''
    if request.method =='GET':
        response = requests.get('https://api.themoviedb.org/3/movie/now_playing?api_key=' \
        + cfg.tmDB['api_key'] + '&language=en-US&page=' + str(page))
        movie_data = response.json()
        trim_unwanted_data_list(movie_data)
        add_consensus_data_list(movie_data)
        return JsonResponse(movie_data)
    return None

def top_rated(request, page):
    '''responsible for the top_rated url call'''
    if request.method =='GET':

        total_results = Movie.objects.count()
        if page > math.ceil(total_results/20):
            return HttpResponseNotFound("")
        total_pages = math.ceil(total_results/20)
        start_index = (page - 1) * 20
        end_index = start_index + 20
        top_rated_list = Movie.objects.order_by('-review_score')[start_index:end_index]
        if top_rated_list:
            top_rated_list = MovieSerializer(top_rated_list, many=True).data
            for top_movie in top_rated_list:
                top_movie['genre_ids'] = top_movie['genre_ids'].split(',')
                for j in range(len(top_movie['genre_ids'])):
                    top_movie['genre_ids'][j] = int(top_movie['genre_ids'][j])
            top_rated_list = {"page":page, "results" : top_rated_list, \
                "total_pages" : total_pages, "total_results" : total_results}
            return JsonResponse(top_rated_list, safe=False)
    return None

def genre(request,genre_id,page):
    '''responsible for the genre url call'''
    if request.method == 'GET':
        response = requests.get('https://api.themoviedb.org/3/discover/movie?api_key=' + \
        cfg.tmDB['api_key'] + '&sort_by=popularity.desc&include_adult=false&include_video' \
        '=false&page='
        + str(page) + '&with_genres=' + str(genre_id))
        movie_data = response.json()
        trim_unwanted_data_list(movie_data)
        add_consensus_data_list(movie_data)
        return JsonResponse(movie_data)
    return None


#DEPRECIATED (March 10, 2021)
# def genre(request,listType, genreID, page):
#     if request.method =='GET':

#         if listType == 'popular':
#                 response = requests.get('https://api.themoviedb.org/3/movie/popular?api_key=' + cfg.tmDB['api_key'] + '&language=en-US&page=' + str(page))
#         elif listType == 'newReleases':
#             requests.get('https://api.themoviedb.org/3/movie/now_playing?api_key=' + cfg.tmDB['api_key'] + '&language=en-US&page=' + str(page))
#         elif listType == 'topRated':
#             #TO BE IMPLEMENTED
#             response = requests.get('https://api.themoviedb.org/3/movie/popular?api_key=' + cfg.tmDB['api_key'] + '&language=en-US&page=' + str(page))
#         else:
#             return HttpResponseNotFound()
        
#         movieData = response.json()

#         #create duplicate of movie data and delete the results to be added back as per the genre filter
#         filteredMovieData = movieData.copy()
#         del filteredMovieData['results']
#         filteredMovieData['results'] = []

#         #adding results to filteredMovieData if they match the desired genre
#         for i in movieData['results']:
#             if genreID in i['genre_ids']:
#                 filteredMovieData['results'].append(i)

#         #remove undesired data and add our own
#         trimUnwantedDataList(filteredMovieData)
#         addConsensusData(filteredMovieData)
#         return JsonResponse(filteredMovieData)   

def details(request, movie_id):
    '''responsible for the details url call'''
    if request.method =='GET':
        response = requests.get('https://api.themoviedb.org/3/movie/' + str(movie_id) + \
        '?api_key=' + cfg.tmDB['api_key'] + '&language=en-US')
        movie_item = response.json()
        trim_unwanted_data_details(movie_item)
        add_consensus_data(movie_item)
        add_movie_videos(movie_item)
        add_movie_credits(movie_item)
        add_movie_top_reddit_posts(movie_item)
        add_movie_common_phrases(movie_item)
        add_movie_common_words(movie_item)
        return JsonResponse(movie_item)
    return None

def search(request):
    '''responsible for the search url call'''
    if request.method =='GET':
        movie_title = request.GET.get('movie_title', None)
        if movie_title is None:
            return HttpResponseNotFound("")
        response = requests.get('https://api.themoviedb.org/3/search/movie?api_key=' + \
        cfg.tmDB['api_key'] + '&language=en-US&query=' + movie_title)
        movie_data = response.json()
        trim_unwanted_data_list(movie_data)
        add_consensus_data_list(movie_data)
        return JsonResponse(movie_data)
    return None


#HELPER FUNCTIONS
def trim_unwanted_data_list(dict_data):
    '''Removes unwanted arguments from every movie in list'''
    for i in dict_data['results']:
        i.pop('adult', None)
        i.pop('original_language', None)
        i.pop('original_title', None)
        i.pop('overview', None)
        i.pop('popularity', None)
        i.pop('video', None)
        i.pop('vote_average', None)
        i.pop('vote_count', None)

def trim_unwanted_data_details(movie_details):
    '''Removes unwanted arguments from single movie details dictionary'''
    movie_details.pop('adult', None)
    movie_details.pop('belongs_to_collection', None)
    movie_details.pop('budget', None)
    movie_details.pop('homepage', None)
    movie_details.pop('imdb_id', None)
    movie_details.pop('original_language', None)
    movie_details.pop('original_title', None)
    movie_details.pop('popularity', None)
    movie_details.pop('production_companies', None)
    movie_details.pop('production_countries', None)
    movie_details.pop('revenue', None)
    movie_details.pop('spoken_languages', None)
    movie_details.pop('status', None)
    movie_details.pop('tagline', None)
    movie_details.pop('vote_average', None)
    movie_details.pop('vote_count', None)

def add_consensus_data_list(dict_data):
    '''adds consensus data to a list of movies'''
    for i in dict_data['results']:
        add_consensus_data(i)


def add_consensus_data (movie_item):
    '''adds consensus data such as id and review percentage to the movie_item given'''
    consensus_movie_object = Movie.objects.filter(tmdb_id = movie_item['id'])
    if consensus_movie_object.count() != 0:
        movie_item['CID'] = consensus_movie_object[0].id
        movie_item['review_percentage'] = consensus_movie_object[0].review_score
        movie_item['number_posts'] = consensus_movie_object[0].number_posts
    else:
        movie_item['CID'] = 0
        movie_item['review_percentage'] = 0
        movie_item['number_posts'] = 0

def add_movie_videos(movie_item):
    '''adds a list with all the videos relating to a movie to the movie_item given'''
    response = requests.get('https://api.themoviedb.org/3/movie/' + str(movie_item['id']) \
    + '/videos' + '?api_key=' + cfg.tmDB['api_key'] + '&language=en-US&')
    videos_item = response.json()
    del videos_item['id']
    movie_item['videos'] = videos_item

def add_movie_credits(movie_item):
    '''adds a list with the entire credits for a movie to the movie_item given'''
    response = requests.get('https://api.themoviedb.org/3/movie/' + str(movie_item['id']) \
    + '/credits' + '?api_key=' + cfg.tmDB['api_key'] + '&language=en-US&')
    credits_item = response.json()
    del credits_item['id']
    del credits_item['crew']
    movie_item['credits'] = credits_item



def add_movie_top_reddit_posts(movie_item):
    '''add our top reddit posts to the movie_item'''
    try:
        top_reddit_posts = Reddit_Post.objects.filter(movie_id = movie_item['CID'])\
            .order_by('-score')[:3]
        if top_reddit_posts:
            top_reddit_posts = RedditPostSerializer(top_reddit_posts, many=True).data
            movie_item['top_reddit_posts'] = top_reddit_posts

    except IndexError as exception:
        logger.error(exception)

def add_movie_common_phrases(movie_item):
    '''add our top common phrases for a movie to the movie_item'''
    try:
        top_phrases = Common_Phrase_Movie.objects.filter(movie_id = movie_item['CID'])\
            .order_by('-count')[:10]
        if top_phrases:
            top_phrases = CommonPhraseMovieSerializer(top_phrases, many=True).data
            movie_item['common_phrases'] = top_phrases

    except IndexError as exception:
        logger.error(exception)

def add_movie_common_words(movie_item):
    '''add our top common words for a movie to the movie_item'''
    try:
        top_words = Common_Word_Movie.objects.filter(movie_id = movie_item['CID'])\
            .order_by('-count')[:10]
        if top_words:
            top_words = CommonWordMovieSerializer(top_words, many=True).data
            movie_item['common_words'] = top_words

    except IndexError as exception:
        logger.error(exception)